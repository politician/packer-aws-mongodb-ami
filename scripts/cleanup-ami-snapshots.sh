#!/bin/sh

printf "Removing old images and snapshots for MongoDB $MONGODB_VERSION and keep only $KEEP_AMI_REVISIONS revisions\n"

images_to_deregister=$(aws ec2 describe-images --owners self --filters "Name=tag:MongoDB_Version,Values=$MONGODB_VERSION" "Name=tag:Packer,Values=true" --query 'reverse(sort_by(Images,&CreationDate))['$KEEP_AMI_REVISIONS':].{ID:ImageId}' --output text)

printf "\nDeregistering images...\n"

# Deregister images
for image in $images_to_deregister; do
  printf "$image"
  aws ec2 deregister-image --image-id $image
  printf " -> Done\n"
done

snapshots_to_delete=$(aws ec2 describe-snapshots --filters "Name=tag:MongoDB_Version,Values=$MONGODB_VERSION" "Name=tag:Packer,Values=true" --query 'reverse(sort_by(Snapshots,&StartTime))['$KEEP_AMI_REVISIONS':].{ID:SnapshotId}' --output text)

printf "\nDeleting snapshots...\n"

# Delete snaphsots
for snap in $snapshots_to_delete; do
  printf "$snap"
  aws ec2 delete-snapshot --snapshot-id $snap
  printf " -> Done \n"
done